output "cluster_issuer_name" {
  value = module.cluster_issuer.cluster_issuer_name
}

output "cluster_subject_organizations" {
  value = var.cluster_subject_organizations
} 

output "cluster_subject_organizationalunits" {
  value = var.cluster_subject_organizationalunits
}