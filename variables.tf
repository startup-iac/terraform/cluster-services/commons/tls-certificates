variable "cluster_instance_name" {
  type        = string
  description = "Cluster instance name"
}

variable "cluster_environments" {
  type        = list(string)
  description = "List of Environment Names."
}

variable "cluster_dns_name" {
  type = string
}

variable "cluster_issuer_server" {
  type = string
}

variable "cluster_issuer_email" {
  type = string
}

variable "cluster_subject_organizations" {
  type = string
} 

variable "cluster_subject_organizationalunits" {
  type = string
}